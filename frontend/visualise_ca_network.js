$(window).ready(function(){
    console.log("Starting up...");
    generate_ca_data_table();
});

function generate_ca_data_table() {
    $("#ca_data_table").html("<span>test</span>");
    var table = $("<table class='table table-hover table-condensed'/>");
    var tbody = $("<tbody class=''/>");
    table.append(tbody);

    var header = $("<tr/>")
        .append($("<th/>").text("Company name"))
        .append($("<th/>").text("Directors"))
        .append($("<th/>").text("Filing history"));

    tbody.append(header);

    var companies = companies_data.companies;
    var officers = companies_data.officers;
    var appointments = companies_data.appointments; 

    for(var companyIdx in companies) {
        console.log("Adding company: " + companies[companyIdx].company_name);
        var company_number = companies[companyIdx].company_number
        var appointments_links = appointments
            .filter(function(appt) { return appt.company_number === company_number })
            .map(function(appt) { return appt.appointments_link });

        var company_officers = officers.filter(function(o) { return appointments_links.findIndex(
            function(al) {return o.appointments_link === al}) >= 0});

        var company_officer_names = company_officers.map(function(o) { return o.officer_name}).join(", ")
        
        
        

        console.log(company_officers);

        var row = $("<tr/>")
            .append($("<td/>").text(companies[companyIdx].company_name))
            .append($("<td/>").text(company_officer_names));
        tbody.append(row);
    }

    $("#ca_data_table").append(table);
}